// Example events structure
// TODO: get this from a form field, and later from Google Calendar API
let events = [
    {
        summary: 'Fucked Up Friday',
        start: '2023-03-03T20:00:00-0500'
    },
    {
        summary: 'Ultraviolet Night',
        start: '2023-03-04T20:00:00-0500'
    },
    {
        summary: 'Scuffed Sunday',
        start: '2023-03-05T16:00:00-0500'
    },
    {
        summary: 'Movie Night',
        start: '2023-03-07T19:30:00-0500'
    },
    {
        summary: 'Fucked Up Friday',
        start: '2023-03-10T20:00:00-0500'
    },
    {
        summary: 'Ultraviolet Night',
        start: '2023-03-11T20:00:00-0500'
    },
    {
        summary: 'Scuffed Sunday',
        start: '2023-03-12T16:00:00-0400'
    },
    {
        summary: 'Fucked Up Friday',
        start: '2023-03-17T20:00:00-0400'
    },
    {
        summary: 'Ultra-NSFW Night',
        start: '2023-03-18T20:00:00-0400'

    },
    {
        summary: 'Cuffed Sunday (NSFW)',
        start: '2023-03-19T16:00:00-0400'
    },
    {
        summary: 'Game Night',
        start: '2023-03-21T19:30:00-0400'
    },
    {
        summary: 'Fucked Up Friday',
        start: '2023-03-24T20:00:00-0400'
    },
    {
        summary: 'Ultraviolet Night',
        start: '2023-03-25T20:00:00-0400'
    },
    {
        summary: 'Scuffed Sunday',
        start: '2023-03-26T16:00:00-0400'
    },
    {
        summary: 'FPS Gauntlet',
        start: '2023-03-31T18:00:00-0400'
    },
    {
        summary: 'Fucked Up Friday',
        start: '2023-03-31T20:00:00-0400'
    }
]

export { events };
