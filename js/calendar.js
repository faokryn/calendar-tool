/*
calendar.js

Exposes generateCalendar() which uses the Canvas API to parametrically generate a monthly calendar.
https://gitlab.com/faokryn/calendar-tool/

Copyright 2023 Colin "faokryn" O'Neill <code@faokryn.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Wrap image load event in a Promise so we can utilize Promise.all and other modern asynch constructs
const loadImage = (src) => new Promise((resolve, reject) => {
    try {
        const img = new Image();
        img.addEventListener('load', () => resolve(img));
        img.src = src;
    }
    catch (exception) {
        console.error(`Failed to load image "${src}`);
        reject(exception);
    }
});

// Create FontFace, add it to documents.fonts FontFaceSet, and return its loaded Promise for consistency with loadImage
const loadFont = (family, src) => new Promise((resolve) => {
    const font = new FontFace(family, `url(${src})`);
    document.fonts.add(font);
    return resolve(font.load());
});

// Wrapper function around common text styling options to simplify styling and ensure styles are reset to defaults
// if left unspecified.
const styleCanvasText = (ctx, font='16px serif', color='black', textBaseline='alphabetic', textAlign='left') => {
    ctx.font = font;
    ctx.fillStyle = color;
    ctx.textAlign = textAlign;
    ctx.textBaseline = textBaseline;
}

// Wrapper function around common stroke styling options to simplify styling and ensure styles are reset to defaults
// if left unspecified.
const styleCanvasStroke = (ctx, color='black', width=1) => {
    ctx.lineWidth = width;
    ctx.strokeStyle = color;
}

// Take a date and return a "Month Title" string (e.g. "March 2023")
const monthTitle = (d) => d.toLocaleString('default', {timeZone: 'UTC', month: 'long'}) + ' ' + d.getFullYear();

// Generate the calendar using the canvas API
const generateCalendar = (canvas, month, events) => {
    /////////////////////////////////
    // DEFINE CONSTANTS, LUTS, ETC //
    /////////////////////////////////

    // Canvas context
    const c = canvas.getContext('2d');

    // Asset sources
    const BACKGROUND_SRC = 'assets/images/background.png';
    const DECORATION_SRC = 'assets/images/background_decoration.png';
    const LOGO_SRC       = 'assets/images/logo.png';
    const TITLE_FONT_SRC = 'assets/fonts/daggersquare.otf';
    const BODY_FONT_SRC  = 'assets/fonts/daggersquare.otf';

    // Sizing constants
    const HEADER_HEIGHT = Math.floor(canvas.height / 8);
    const DAYBAR_HEIGHT = Math.floor(canvas.height / 16);
    const ROW_HEIGHT    = Math.floor(canvas.height / 8);
    const INNER_MARGIN  = Math.floor(canvas.height / 64);
    const OUTER_MARGIN  = Math.floor(canvas.height / 32);

    // Color lookup table
    const colorLUT = {
        'Off white':    '#eeeeee',
        'Light gray':   '#bfbfbf',
        'Off black':    '#212121',
        'PE orange':    '#ff6815',
        'PE purple':    '#af31ff',
        'PE blue':      '#0600ff'
    }

    // Functions

    // Styling functions
    // Allows us to use the styling wrappers without specifying the context every time.
    // Also allows them to be called without arguments to reset all styles to defaults.
    const styleText = (...args) => styleCanvasText(c, ...args);
    const styleStroke = (...args) => styleCanvasStroke(c, ...args);

    // Drop shadow functions
    // Wrap setting the shadowColor to more descriptively enable and disable drop shadows
    const dropshadowOn = () => c.shadowColor = colorLUT['Off black'];
    const dropshadowOff = () => c.shadowColor = '#00000000';

    ////////////////////////
    // GET MONTH & EVENTS //
    ////////////////////////

    const monthDate = new Date(month);

    // Use the array of events for the month to generate an array of days where each day is represented as an array of
    // events (days without events are represented by an empty array) such that the 0th element of the array is the 1st
    const days = events.reduce(
        (r, e) => {
            const start = new Date(e.start);

            // double check events are for the specified month
            if (start.getFullYear() === monthDate.getUTCFullYear() && start.getMonth() === monthDate.getUTCMonth()) {
                r[start.getDate() - 1] = [
                    ...r[start.getDate() - 1],
                    {
                        name: e.summary,
                        // get time in Eastern (no matter the input) in format H:MM AM/PM ZZZ
                        time: start.toLocaleString('en-US', {
                            hour: 'numeric',
                            minute: 'numeric',
                            timeZoneName: 'short',
                            timeZone: 'America/New_York'
                        })
                    }
                ]
            }
            return r;
        },
        Array((new Date(monthDate.getUTCFullYear(), monthDate.getUTCMonth() + 1, 0)).getUTCDate()).fill().map(e => [])
    );

    /////////////////////
    // RENDER CALENDAR //
    /////////////////////

    Promise.all([
        loadImage(BACKGROUND_SRC),
        loadImage(DECORATION_SRC),
        loadImage(LOGO_SRC),
        loadFont('titlefont', TITLE_FONT_SRC),
        loadFont('bodyfont', BODY_FONT_SRC),
    ]).then(([background, decoration, logo]) => {

        // set up drop shadow style
        c.shadowOffsetX = INNER_MARGIN/4;
        c.shadowOffsetY = INNER_MARGIN/4;
        c.shadowBlur = INNER_MARGIN/2;

        // render background
        c.fillStyle = colorLUT['PE purple'];
        c.fillRect(0, 0, canvas.width, canvas.height);
        c.drawImage(background, 0, 0, canvas.width, canvas.height);
        dropshadowOn();
        c.drawImage(decoration, 0, 0, canvas.width, canvas.height);

        // render header
        {
            const LOGO_WIDTH = HEADER_HEIGHT * logo.width / logo.height;
            const TITLE_SIZE = 3/4*HEADER_HEIGHT;

            // logo
            c.drawImage(logo, OUTER_MARGIN, OUTER_MARGIN, LOGO_WIDTH, HEADER_HEIGHT);

            // title
            const title = [
                monthTitle(monthDate),
                OUTER_MARGIN + LOGO_WIDTH + 2*INNER_MARGIN,
                OUTER_MARGIN
            ];
            // generate the title's style by layering three fill styles:
            //      - solid color, which gets a drop shadow
            //      - a dark to transparent vertical gradient,
            //      - and an angled "metallic shine" to approximate the logo's style
            [
                // solid color
                colorLUT['PE orange'],
                // dark vertical gradient
                (() => {
                    const g = c.createLinearGradient(0, OUTER_MARGIN, 0, OUTER_MARGIN + HEADER_HEIGHT);
                    g.addColorStop(0.0, '#00000000');
                    g.addColorStop(0.3, '#00000000');
                    g.addColorStop(1.0, '#00000080');
                    return g;
                })(),
                // "metallic shine" gradient
                (() => {
                    const SHINE_RAD = 0.5; // 0.5 radians
                    const g = c.createLinearGradient(0, 0, canvas.width, canvas.width * Math.tan(SHINE_RAD));
                    Array(7).fill().map((e, i) => (i + 1.5)/10).forEach(e => {
                        g.addColorStop(e, '#ffffff00');
                        g.addColorStop(e, '#ffffffA0');
                        g.addColorStop(e + 0.025, '#ffffffA0');
                        g.addColorStop(e + 0.025, '#ffffff00');
                    });
                    return g;
                })()
            ].forEach((e, i) => {
                if (i === 0) dropshadowOn();
                else dropshadowOff();

                styleText(`italic ${TITLE_SIZE}px titlefont`, e, 'hanging');
                c.fillText(...title);
            });
            styleStroke(colorLUT['PE blue'], TITLE_SIZE/64);
            dropshadowOff();
            c.strokeText(...title);

            // subtitle
            styleText(`italic ${HEADER_HEIGHT - TITLE_SIZE}px titlefont`, colorLUT['Off white'], 'hanging');
            dropshadowOn();
            c.fillText(
                'Event Schedule',
                OUTER_MARGIN + LOGO_WIDTH + 2*INNER_MARGIN,
                OUTER_MARGIN + TITLE_SIZE
            );
        }

        {
            const COLUMN_WIDTH = (canvas.width - 2*OUTER_MARGIN + INNER_MARGIN) / 7;

            // render day bar
            ['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY'].forEach((e, i) => {
                // box
                c.beginPath();
                c.roundRect(
                    OUTER_MARGIN + i*COLUMN_WIDTH,
                    OUTER_MARGIN + HEADER_HEIGHT + INNER_MARGIN,
                    COLUMN_WIDTH - INNER_MARGIN,
                    DAYBAR_HEIGHT - INNER_MARGIN,
                    DAYBAR_HEIGHT / 8
                );
                dropshadowOn();
                c.fillStyle = colorLUT['Off white'];
                c.fill();
                c.fillStyle = (() => {
                    const g = c.createLinearGradient(
                        0,
                        OUTER_MARGIN + HEADER_HEIGHT,
                        0,
                        OUTER_MARGIN + HEADER_HEIGHT + DAYBAR_HEIGHT
                    );
                    g.addColorStop(1, '#00000040');
                    g.addColorStop(0, '#00000000');
                    return g;
                })();
                c.fill();

                // label
                dropshadowOff();
                styleText(`bold ${DAYBAR_HEIGHT/2}px titlefont`, colorLUT['Off black'], 'middle', 'center');
                c.fillText(
                    e,
                    OUTER_MARGIN + i*COLUMN_WIDTH + (COLUMN_WIDTH - INNER_MARGIN)/2,
                    OUTER_MARGIN + HEADER_HEIGHT + INNER_MARGIN + (DAYBAR_HEIGHT - INNER_MARGIN)/2,
                    COLUMN_WIDTH - 2*INNER_MARGIN
                );
            });

            // render day cells
            {
                const CELL_PADDING = ROW_HEIGHT / 16;
                const DATE_SIZE = Math.floor(ROW_HEIGHT / 5);
                const CELL_TEXT_SIZE = (ROW_HEIGHT - 2*CELL_PADDING)/5;

                days.forEach((e, i) => {
                    const CELL_POSITION = i + ((monthDate.getUTCDay() + 6) % 7); // Monday first
                    const CELL_X = COLUMN_WIDTH * (CELL_POSITION % 7) + OUTER_MARGIN;
                    const CELL_Y = ROW_HEIGHT * Math.floor(CELL_POSITION / 7)
                        + HEADER_HEIGHT + DAYBAR_HEIGHT + OUTER_MARGIN + INNER_MARGIN;

                    // cell body
                    c.beginPath();
                    c.roundRect(
                        CELL_X,
                        CELL_Y,
                        COLUMN_WIDTH - INNER_MARGIN,
                        ROW_HEIGHT - INNER_MARGIN,
                        CELL_PADDING
                    );
                    dropshadowOn();
                    styleStroke(colorLUT['Light gray']);
                    c.stroke();
                    dropshadowOff();
                    c.fillStyle = e.length ? colorLUT['Light gray'] + '80' : colorLUT['Off black'] + '40';
                    c.fill();
                    c.fillStyle = (() => {
                        const g = c.createLinearGradient(0, CELL_Y, 0, CELL_Y + ROW_HEIGHT - INNER_MARGIN);
                        g.addColorStop(1, '#00000040');
                        g.addColorStop(0, '#00000000');
                        return g;
                    })();
                    c.fill();

                    // date label
                    styleText(`${DATE_SIZE}px titlefont`, colorLUT['PE orange'], 'hanging');
                    c.fillText(i + 1, CELL_X + CELL_PADDING, CELL_Y + CELL_PADDING);

                    // event text

                    // For now, we'll only support up to two events per day, and warn if exceeded
                    if (e.length > 2) {
                        console.warn(
                            'This tool supports a maximum of two events per day. The first two events will be used for '
                            + `${monthDate.toLocaleString('default', {month: 'long', timeZone: 'UTC'})} ${i + 1}. `
                            + 'You may need to manually edit the final image to satisfy your requirements.'
                        );
                        e = e.slice(0, 2);
                    }

                    styleText(`${CELL_TEXT_SIZE}px bodyfont`, 'black', 'bottom', 'right');
                    e.reverse().forEach((e, i) => {
                        const SECOND_EVENT_OFFSET = i*(2*CELL_TEXT_SIZE + CELL_PADDING);
                        c.fillText(
                            e.time,
                            CELL_X + COLUMN_WIDTH - INNER_MARGIN - CELL_PADDING,
                            CELL_Y + ROW_HEIGHT - INNER_MARGIN - CELL_PADDING - SECOND_EVENT_OFFSET
                        );
                        c.fillText(
                            e.name,
                            CELL_X + COLUMN_WIDTH - INNER_MARGIN - CELL_PADDING,
                            CELL_Y + ROW_HEIGHT - INNER_MARGIN - CELL_PADDING - CELL_TEXT_SIZE - SECOND_EVENT_OFFSET,
                            COLUMN_WIDTH - INNER_MARGIN - 2*CELL_PADDING
                        );
                    });
                });
            }
        }

    }).catch((exception) => {
        console.error(exception)
    });
}

// Generate locale-specific text in Discord's relative time format
const generateDiscordText = (target, month, events) => {
    const monthDate = new Date(month);
    let result = "";

    // add title
    result += "__**Events for " + monthTitle(monthDate) + "**__\n\n";

    // add events
    result += events.map(
        e => `• &lt;t:${Math.floor((new Date(e.start)).valueOf() / 1000)}:F&gt;: ${e.summary}\n`
    ).join('');

    // add note
    result += '\n_* dates and times above are in your local time based on Discord\'s locale settings_';

    target.innerHTML = result;
}

export { generateCalendar, generateDiscordText }

// ${Math.floor(monthDate.valueOf() / 1000)}
