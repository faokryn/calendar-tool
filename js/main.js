/*
main.js

Handles instantiating dynamic default values for form data, reading form data, and setting up event handlers for buttons
https://gitlab.com/faokryn/calendar-tool/

Copyright 2023 Colin "faokryn" O'Neill <code@faokryn.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import { generateCalendar, generateDiscordText } from "./calendar.js";

const main = () => {
    // get the required DOM elements
    const calMonth = document.getElementById('calMonth');
    const calEvents = document.getElementById('calEvents');
    const calendar = document.getElementById('calendar');
    const discordOutput = document.getElementById('discord-output');
    const generateBtn = document.getElementById('generateBtn');
    const exportBtn = document.getElementById('exportBtn');

    // set default value of calMonth to the next month
    calMonth.value = (new Date()).toISOString().slice(0,7).split('-')
        .map((e, i) => i === 0 ? e : (parseInt(e, 10) + 1).toString().padStart(2, '0')).join('-');

    // "Generate" button event handler
    generateBtn.addEventListener('click', () => {
        // try to read the events JSON, but generate an empty calendar if we can't
        let events = [];
        try {
            events = JSON.parse(calEvents.value);
        }
        catch (exception) {
            console.error('Could not parse events JSON: ' + exception);
        }
        generateCalendar(calendar, calMonth.value, events);
        generateDiscordText(discordOutput, calMonth.value, events);

        // display the canvas
        calendar.style = 'display: block';

        // enable the "Export" button
        exportBtn.disabled = false;
    });

    // "Export" button event handler
    exportBtn.addEventListener('click', () => {
        const a = document.createElement('a');
        // get the content of the canvas as a png
        a.href = calendar.toDataURL('image/png');
        // set the downloaded file's name
        a.download = calMonth.value + '_calendar.png';
        // download the file
        a.click();
    });
}

window.onload = main;
