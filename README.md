# Calendar Tool

This tool is intended to parametrically generate a monthly calendar from a month string in YYYY-MM format and a JSON list of events for that month. The format of the JSON string should be:

```
[
    {
        "summary": "Event 1 Name",
        "start": "<ISO 8601 datetime string>"
    },
    {
        "summary": "Event 2 Name",
        "start": "<ISO 8601 datetime string>"
    },
    // ...
    {
        "summary": "Event N Name",
        "start": "<ISO 8601 datetime string>"
    }
]
```

I'm developing this tool to help create calendars for a group with which I am involved, so it currently has some opinionated features for generating that group's calendars. I may generalize it further in the future. Feel free to use it as a starting point for your own calendar creation tools. You can contact me at code@faokryn.com

The code is licensed under the Apache License 2.0. A copy of the full License text should be distributed with the source code, and can also be obtained at http://www.apache.org/licenses/LICENSE-2.0
